<!-- Header-Menu Section start -->
<?php 
function header_menu(){
 ?>
	<div class="advancedLogin fullWidth  borderRadius">
            <div class="rowAdvanced">
                <div class="col-md-8">
                    <div class="logo borderRadius bgGray24 ">
                        <img src="img/logo (1).png" class="imgLogo">
                    </div>
                </div>
                <div class="col-md-4 ">
                    <div class="homePage floatLeft borderRadius bgGray24">
                        <i class="fa fa-laptop fa-3x colorlightCoral iconPosition"></i>
                        <h4 class="colorlightCoral textPosition">Home Page</h4>
                    </div>
                    <div class="contactUs  borderRadius bgGray24">
                        <i class="fa fa-envelope-o fa-3x colorlightCoral iconPosition"></i>
                        <h4 class="colorlightCoral textPosition">Contact Us</h4>
                    </div>
                </div>
            </div>
        </div>
<?php 
}
 ?>
<!-- Header-Menu Section end -->

<!-- Hint Section start -->
<?php 
function hint(){
?>
<div class="welcomeClient fullWidth marginTop border bgwhiteSmokey">
	<i class="colordarkGray floatLeft fa fa-microphone fa-3x"></i>
	<p class="colordarkGray">22. Nov. 2013 <b>Welcome to our Client Area!</b></p>
	<p class="colordarkGray">We are pleased to announce the new responsive release of Advanced Login System v 2.5</p>
</div>
<?php 
}
 ?>
<!-- Hint Section end -->

<!-- login section -->
<?php 
function login(){
?>
        <form class="logIn marginTop fullWidth" action="login.php" method="post">
            <!-- user login -->
            <div class="userLogin fullWidth bgwhiteSmokey">
                <h4>User Login</h4>
            </div>
            <!-- login and password -->
            <div class="userPassword fullWidth
    bgwhiteSmokey">
                <div class="rowUser">
                    <div class="col-md-3">
                        <h4>Username</h4>
                    </div>
                    <div class="col-md-9">
                        <div class="input">
                            <i class="colorwhiteSmokey fa fa-user"></i>
                            <input type="text" name="username" placeholder="username">
                            <i class="colorwhiteSmokey fa fa-asterisk"></i>
                        </div>
                    </div>
                </div>
                <div class="gridPass">
                    <div class="rowPass">
                        <div class="col-md-3">
                            <h4>Password</h4>
                        </div>
                        <div class="col-md-9">
                            <div class="input">
                                <i class="colorwhiteSmokey fa fa-user"></i>
                                <input type="text" name="password" placeholder="password">
                                <i class="colorwhiteSmokey fa fa-asterisk"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="buttonSection fullWidth bgwhiteSmokey">
                <div class="rowButton">
                    <div class="col-md-8">
                        <button class="borderRadius passReq colorWhite">Password Request
                        </button>
                    </div>
                    <div class="col-md-4">
                        <button class="borderRadius regAccount colorWhite bgGray24" type="submit">Register Account
                        </button>
                        <input class="colorWhite borderRadius loginNow" type="submit" value="Login Now">
                    </div>
                </div>
            </div>
        </form>
<?php 
}
?>